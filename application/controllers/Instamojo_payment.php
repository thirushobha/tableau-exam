<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Instamojo_Payment extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('instamojo');
		$this->load->helper('url');
		$this->load->library('form_validation','ion_auth','session');
		$this->load->model('base_model');
			$this->validate_normaluser();
	}

	public function payment_probilem()
	{
		
		
		$this->load->view("front/header",$this->data);
        $this->load->view('payment/payment_problem',$this->data, FALSE);
        $this->load->view("front/footer",$this->data);
	}
	public function payment_success($catid='',$subcatid='')
	{
		$this->data['category'] = $this->db->get_where('categories',array('catid'=>$catid))->row_array();
		if($subcatid!=""){
      	$this->data['subcategory'] = $this->db->get_where('subcategories',array('subcatid'=>$subcatid))->row_array();
      }else{
      	$this->data['subcategory'] = '';
      }
      	$this->data['user_details'] = $this->ion_auth->get_user_details();
		$this->load->view("front/header",$this->data);
        $this->load->view('payment/payment_success',$this->data, FALSE);
        $this->load->view("front/footer",$this->data);
	}


	public function index($amount='',$catid='',$subcatid='')
	{
		$user_details = $this->ion_auth->get_user_details();

		if($catid==""){
			redirect('instamojo_payment/payment_probilem','refresh');
		}
		if($subcatid!=''){
			$is_category = 0;
			$subcatidd = $subcatid;
		}else{
			$is_category = 1;
			$subcatidd = '';
		}
		$insert_payment_data = array(
			'user_id_payment'=>$user_details->id,
			'is_category_payment'=>$is_category,
			'catid_payment'=>$catid,
			'subcatid_payment'=>$subcatidd,
			'amount_payment'=>$amount,
		);

		$this->session->set_userdata($insert_payment_data);
		
		$pay = $this->instamojo->pay_request( 

						$amount = $amount , 
						$purpose = "Purchase of Course" , 
						$buyer_name = $user_details->username , 
						$email = $user_details->email , 
						$phone = $user_details->phone ,
		     			$send_email = 'TRUE' , 
		     			$send_sms = 'TRUE' , 
		     			$repeated = 'FALSE'

		     		);

		$redirect_url = $pay['longurl'];
		redirect($redirect_url,'refresh') ;

	}

		public function status()
	{
		$payment_id = $this->input->get('payment_id');
		$payment_status = $this->input->get('payment_status');
		$payment_request_id = $this->input->get('payment_request_id');

		$status     = $this->instamojo->status($payment_request_id);

		if($status['status']=="Completed")
		{
			if($status['payments'][0]['status']=='Credit')
			{
				$user_id_payment = $this->session->userdata('user_id_payment');
				$is_category_payment = $this->session->userdata('is_category_payment');
				$catid_payment = $this->session->userdata('catid_payment');
				$subcatid_payment = $this->session->userdata('subcatid_payment');
				$amount_payment = $this->session->userdata('amount_payment');

				$insert_payment_data = array(
			'user_id'=>$user_id_payment,
			'is_category'=>$is_category_payment,
			'catid'=>$catid_payment,
			'subcatid'=>$subcatid_payment,
			'amount'=>$amount_payment,
			'payment_id'=>$payment_id,
			'payment_request_id'=>$payment_request_id,
			'payment_status'=>$payment_status
		);
				$id = $this->base_model->insert_operation_id($insert_payment_data,'course_subscription');
				if($id>0)
				{
					redirect('instamojo_payment/payment_success/'.$catid_payment.'/'.$subcatid_payment,'refresh');

				}else{
					redirect('instamojo_payment/payment_probilem','refresh');
				}

			}else{
				redirect('instamojo_payment/payment_probilem','refresh');
			}

		}else{
			redirect('instamojo_payment/payment_probilem','refresh');

		}



		print_r($status);
	}

	public function pay_request()
	{
		
		$pay = $this->instamojo->pay_request( 

						$amount = "200" , 
						$purpose = "TEST" , 
						$buyer_name = "rbbqq" , 
						$email = "thirupathi.013@gmail.com" , 
						$phone = "8639952863" ,
		     			$send_email = 'TRUE' , 
		     			$send_sms = 'TRUE' , 
		     			$repeated = 'FALSE'

		     		);


		// $payment_id = $pay['id'];  // <= Payment Id
							      // print_r($pay) ; <=  Prints all the data from the request
		$redirect_url = $pay['longurl']   ;
		


		redirect($redirect_url,'refresh') ;

	}

	public function get_all()
	{
		$result = $this->instamojo->all_payment_request();

		print_r($result);
	}







	public function payment_status()
	{
		$requestId = '84c04c212ccb4a8ba8c87e35ec4a2511'  ;
		$status    = $this->instamojo->status($requestId);

		print_r($status) ;
	}


	public function show()
	{
		$data['request_id'] = '7cb82c9fe7104307a7e2bc240c1cf641 ' ;
		$this->load->view('instamojo' ,$data);
	}

}

/* End of file example.php */
/* Location: ./application/controllers/example.php */