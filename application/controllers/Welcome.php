<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {

/*
| -----------------------------------------------------
| PRODUCT NAME: 	DIGI ONLINE EXAMINITION SYSTEM (DOES)
| -----------------------------------------------------
| AUTHER:			DIGITAL VIDHYA TEAM
| -----------------------------------------------------
| EMAIL:			digitalvidhya4u@gmail.com
| -----------------------------------------------------
| COPYRIGHTS:		RESERVED BY DIGITAL VIDHYA
| -----------------------------------------------------
| WEBSITE:			http://digitalvidhya.com
|                   http://codecanyon.net/user/digitalvidhya      
| -----------------------------------------------------
|
| MODULE: 			General
| -----------------------------------------------------
| This is general module controller file.
| -----------------------------------------------------
*/

	//Load the Parent Constructor in Welcome Class Constructor and inherit all the properties. And Load any libraries in this Constructor.
	function __construct()
    {
        parent::__construct();
		$this->load->library('form_validation','ion_auth');
		$this->load->helper('url');
    } 
	
	//Home Page (Default Function. If no function is called, this function will be called).
	public function index()
	{
	    $this->data['message'] 			= (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] 	= array(
				'name' 					=> 'identity',
				'id' 					=> 'identity',
				'class'					=> 'form-control',
				'placeholder'			=> 'User Email',
				'type' 					=> 'text',
				'required'				=> 'true',
				'value' 				=> $this->form_validation->set_value('identity'),
			);
			$this->data['password'] 	= array(
				'name' 					=> 'password',
				'id' 					=> 'password',
				'class' 				=> 'form-control',
				'placeholder' 			=> 'Password',
				'type' 					=> 'password',
				'required' 				=> 'true'
			);
	
		//Latest Quizzes
		$table 							= $this->db->dbprefix('quiz');
		$latest_quizzes 				= $this->base_model->run_query(
		"select quizid,quiztype,name,difficultylevel,deauration,
		startdate,enddate from ".$table." where status='Active' and 
		enddate>='".date('Y-m-d')."' ORDER BY quizid DESC LIMIT 10"
		);
		
		//Notifications
		$table 							= $this->db->dbprefix('notifications');
		$notifications 					= $this->base_model->run_query("select * from "
		.$table." where status = 'Active' and last_date>='"
		.date('Y-m-d')."' ORDER BY nid DESC LIMIT 10"
		);
		
		//Testimonials
		$table 							= $this->db->dbprefix('testimonials');
		$testimonials 					= $this->base_model->run_query("select * from "
		.$table." where status = 'Active' ORDER BY tid DESC"
		);
		
		$this->data['latest_quizzes'] 	= $latest_quizzes;
		$this->data['notifications'] 	= $notifications;
		$this->data['testimonials'] 	= $testimonials;
		$this->data['active_menu'] 		= 'home';
		$this->data['content'] 			= 'home/index';
		$this->_render_page('front/template', $this->data);
	}

	//Render Contact Page
	function contact(){
		$this->data['active_menu'] 		= 'contactus';
		$this->data['content'] 			= 'general/contactus';
		$this->_render_page('front/template', $this->data);
	}
	
	//Send Contact Query to Admin and Send success alert to User in Mail Formats.
	function contact_request_sent()
	{
		if ($this->input->post('submit') != '') {
			$this->load->library('form_validation');
			$this->form_validation->set_rules(
			'name', 
			$this->lang->line('contact_form_validation_name_label'), 
			'required|xss_clean'
			);
			$this->form_validation->set_rules(
			'email', 
			$this->lang->line('contact_form_validation_email_label'), 
			'required|valid_email'
			);
			$this->form_validation->set_rules(
			'phone', 
			$this->lang->line('contact_form_validation_phone_label'), 
			'required|xss_clean|integer'
			);
			$this->form_validation->set_rules(
			'address', 
			$this->lang->line('contact_form_validation_address_label'), 
			'trim|required|xss_clean'
			);
			$this->form_validation->set_rules(
			'subject', 
			$this->lang->line('contact_form_validation_subject_label'), 
			'required|xss_clean'
			);
			if ($this->form_validation->run() == true) {
				$name 		= $this->input->post('name');
				$email 		= $this->input->post('email');
				$phone 		= $this->input->post('phone');
				$address 	= $this->input->post('address');
				$subject 	= $this->input->post('subject');
				$msg 		= "";
				
				if(trim($this->input->post('msg')) != '') {
					$msg 	= $this->input->post('msg');
				}
				$contact_email = $this->base_model->run_query("select contact_email FROM  general_settings ");
				
				//Load Email Library
				$this->load->library('email');
				
				// mail to admin from contactus form				
				$config['charset'] 		= 'utf-8';
				$config['newline'] 		= "\r\n";
				$config['mailtype'] 	= 'html';		
				$config['wordwrap'] 	= TRUE;				
				$this->email->initialize($config);
				$this->email->from($email);		
				$this->email->to($contact_email[0]->contact_email);				
				$this->email->bcc('samson@conquerorstech.net');
				$this->email->subject('Contactus Query');
				$message 				= 'Hello <b>Admin</b>, <br><br>';
				$message 				.='You got a query from <br>Name:<b>'.$name."</b>";
				$message 				.='<br>Phone:<b>'.$phone."</b>";
				$message 				.= '<br>Email:<b>'.$email."</b>";
				$message 				.='<br>Address:<b>'.$address."</b>";
				$message 				.='<br>Subject:<b>'.$subject."</b>";
				if($msg != '')
				$message 				.='<br>Message / Comments:<b>'.$msg."</b>";
				$this->email->message( $message );			
				$this->email->send();
			
				// mail to client from admin form				
				$config['mailtype'] 	= 'html';		
				$config['charset'] 		= 'utf-8';		
				$config['wordwrap'] 	= TRUE;				
				$this->email->initialize($config);
				$this->email->from(
				$contact_email[0]->contact_email, 
				'Digital Online Examination System'
				);
				$this->email->to($email);				
				$this->email->subject('Contact Query Received');
				$message 				= 'Hello <b>'.$name.'</b>, <br><br>';
				$message 				.='Thanks for your interest in Digital 
				Online Examination System. One of our team members 
				will contact you shortly.';
				$message 				.= '<br><br>Thank you,<br><a href="'.base_url().'">DOES</a>';
				$this->email->message($message);			
				$this->email->send();
				$this->prepare_flashmessage(
				'Your contact query sent successfully. Please check your mail.', 
				'0'
				);
				redirect('welcome/contact', 'refresh');
			}
			else {
				$this->prepare_flashmessage(validation_errors(), '1');
				redirect('welcome/contact', 'refresh');
			}
		}
		else {
			redirect('welcome/contact', 'refresh');
		}	
	}
	
	//Render Aboutus Page.
	function aboutus()
	{
		$aboutus_content 				= $this->base_model->run_query(
		"select * from ".$this->db->dbprefix('aboutus_content').""
		);
		$this->data['aboutus_content'] 	= $aboutus_content;
		$this->data['active_menu'] 		= 'aboutus';
		$this->data['content'] 			= 'home/aboutus';
		$this->_render_page('front/template', $this->data);
	}
	
	//Render Terms and Conditions Page.
	function termsConditions()
	{
	    $this->data['content'] 			= 'home/terms_conditions';
		$this->_render_page('front/template', $this->data);
	}
	
	//Render List of Notifications 
	function notifications()
	{
		$table 							= 'notifications';
		$condition 						= '';
		if ($this->uri->segment(3)) {
			$notificationId 			= $this->uri->segment(3);
			$condition['nid'] 			= $notificationId;			
		}
		$notifications 					= $this->base_model->fetch_records_from(
		$table, 
		$condition, 
		$select 						= '*', 
		$order_by 						= ''
		);
		$this->data['notifications'] 	= $notifications;
		if ($this->uri->segment(3)) {
			$this->data['notificationTitle'] = $notifications[0]->title;
		}
		$this->data['content'] 			= 'home/notifications';
		$this->_render_page('front/template', $this->data);
	}
	
	//Check Duplicate Email
	function check_duplicate_email()
	{
		if ($this->base_model->check_duplicate(
		'users', 
		'email', 
		$this->input->post('emailid')
		)) {
			echo "false";
		}
		else {
			echo "true";
		}
	}

	public function courses($catid=''){
		if($catid!=''){
		$this->validate_normaluser();
		$this->data['category'] = $this->db->get_where('categories',array('catid'=>$catid))->row_array();
		$this->data['subcategories'] = $this->db->get_where('subcategories',array('catid'=>$catid))->result_array();
		$userid = $this->ion_auth->get_user_id();
		$where3 = array('catid'=>$catid,'user_id'=>$userid,'is_Category'=>1);
      $enrolled_entire_course = $this->db->get_where('course_subscription',$where3);

      
      if($enrolled_entire_course->num_rows()>0)
      {
      	$this->data['enrolled_entire_course'] = TRUE;

      }else{
      		$this->data['enrolled_entire_course'] = FALSE;
      }

    	$this->load->view("front/header",$this->data);
        $this->load->view("home/courses",$this->data);
        $this->load->view("front/footer",$this->data);
    }else{
    	$this->data['category'] = $this->db->get('categories')->result_array();
    	$this->load->view("front/header",$this->data);
        $this->load->view("home/courseslist",$this->data);
        $this->load->view("front/footer",$this->data);
    }
      }

     
      public function singlecourse($catid,$subcatid){
      	$this->validate_normaluser();
      	$this->data['category'] = $this->db->get_where('categories',array('catid'=>$catid))->row_array();
      	$this->data['subcategory'] = $this->db->get_where('subcategories',array('subcatid'=>$subcatid))->row_array();

      	$topics = $this->db->get_where('subjects',array('catid'=>$catid,'subcatid'=>$subcatid))->result_array();
      	$userid = $this->ion_auth->get_user_id();

      	$this->data['topics'] = $topics;
      	if(count($topics)>0){

      	$where = array('catid'=>$catid,'subcatid'=>$subcatid,'subject_id'=>$topics[0]['subjectid'],'user_id'=>$userid);
      	$exist_id = $this->db->get_where('user_course_completions',$where)->num_rows();
      	if($exist_id<=0){
      		$this->db->insert('user_course_completions',$where);

      	}

      	$where1 = array('catid'=>$catid,'subcatid'=>$subcatid,'user_id'=>$userid);
      	$this->db->select('subject_id');
      	$this->db->order_by('id','DESC');
      	$topics_activate = $this->db->get_where('user_course_completions',$where1)->result_array();
      	foreach ($topics_activate as $key => $value) {
      		$new_arr[] = $value['subject_id'];
      	}
      	$this->data['topics_activate'] = $new_arr;

      }

      $where3 = array('catid'=>$catid,'user_id'=>$userid,'is_category'=>1);
      $enrolled_entire_course = $this->db->get_where('course_subscription',$where3);

      
      if($enrolled_entire_course->num_rows()>0)
      {
      	$this->data['enrolled_entire_course'] = TRUE;

      }else{
      		$this->data['enrolled_entire_course'] = FALSE;
      }



      $where2 = array('catid'=>$catid,'subcatid'=>$subcatid,'user_id'=>$userid);
      $enrolled_course = $this->db->get_where('course_subscription',$where2);


      if($enrolled_course->num_rows()>0)
      {
      	$this->data['enrolled_course'] = TRUE;

      }else{
      		$this->data['enrolled_course'] = FALSE;
      }
       $where_cond_certificate = array('catid'=>$catid,'subcatid'=>$subcatid,'user_id'=>$userid,'display_quizzes'=>1);

      $final_quizzes = $this->db->get_where('user_course_certificate',$where_cond_certificate);


      if($final_quizzes->num_rows()>0)
      {
      	$this->data['display_final_quizzes'] = TRUE;

      }else{
      		$this->data['display_final_quizzes'] = FALSE;
      }



      $this->data['quizzes_count']  = $this->db->get_where('quiz',array('catid'=>$catid,'subcatid'=>$subcatid))->num_rows();
      $this->data['lectures_count']  = $this->db->get_where('subjects',array('catid'=>$catid,'subcatid'=>$subcatid))->num_rows();

      $this->data['final_quizzes']  = $this->db->get_where('quiz',array('catid'=>$catid,'subcatid'=>$subcatid,'is_final'=>1))->result_array();


      $this->data['final_quizzes_finished']  = $this->db->get_where('user_course_certificate',array('catid'=>$catid,'subcatid'=>$subcatid,'user_id'=>$userid))->row_array();




      
      
    	$this->load->view("front/header",$this->data);
        $this->load->view("home/single-course",$this->data);
        $this->load->view("front/footer",$this->data);
      }

      public function topics($catid,$subcatid,$subjectid){
      	
		// $this->validate_normaluser();

      	$this->data['category'] = $this->db->get_where('categories',array('catid'=>$catid))->row_array();
      	$this->data['subcategory'] = $this->db->get_where('subcategories',array('subcatid'=>$subcatid))->row_array();
      	$this->data['subjects'] = $this->db->get_where('subjects',array('subjectid'=>$subjectid))->row_array();


      	$userid = $this->ion_auth->get_user_id();
      	$where = array('catid'=>$catid,'subcatid'=>$subcatid,'subject_id'=>$subjectid,'user_id'=>$userid);
      	$exist_id = $this->db->get_where('user_course_completions',$where)->num_rows();
      	if($exist_id<=0){
      		// $this->db->insert('user_course_completions',$where);
      		redirect('welcome/singlecourse/'.$catid.'/'.$subcatid,'refresh');

      	}



      // 	      $where3 = array('catid'=>$catid,'user_id'=>$userid,'is_category'=>1);
      // $enrolled_entire_course = $this->db->get_where('course_subscription',$where3);

      
      // if($enrolled_entire_course->num_rows()>0)
      // {
      // 	$this->data['enrolled_entire_course'] = TRUE;

      // }else{
      // 		 $where2 = array('catid'=>$catid,'subcatid'=>$subcatid,'user_id'=>$userid,'is_category'=>0);
      // $enrolled_course = $this->db->get_where('course_subscription',$where2);


      // if($enrolled_course->num_rows()>0)
      // {
      // 	$this->data['enrolled_course'] = TRUE;

      // }else{
      // 		redirect('welcome/singlecourse/'.$catid.'/'.$subcatid,'refresh');
      // }

      // }



      	$this->data['topics_activate'] = $this->db->get_where('user_course_completions',$where)->row_array();
      	

    	$this->load->view("front/header",$this->data);
        $this->load->view("home/single-topic",$this->data);
        $this->load->view("front/footer",$this->data);
      }

      public function videostatus_update()
      {
      	$catid = $this->input->post('catid');
      	$subcatid = $this->input->post('subcatid');
      	$subjectid = $this->input->post('subjectid');
      	$userid = $this->ion_auth->get_user_id();
      	$where = array('catid'=>$catid,'subcatid'=>$subcatid,'subject_id'=>$subjectid,'user_id'=>$userid);
      	$exist_id = $this->db->get_where('user_course_completions',$where)->num_rows();
      	$result = array();
      	if($exist_id>0){
      		$update_data = array('video_status'=>'1');
      		$this->db->update('user_course_completions',$update_data,$where);
      		
      		$result['msg'] = "Yes You Watched Total Video then take test";
      	}else{
      		$result['msg'] = "First You Watch Video";
      	}
      	echo json_encode($result);



      }











      //Download the Certificate for the Quiz which consists of best score 
	// among all attempts for the quiz.
	function certificate()
	{
		if ($this->uri->segment(3)) {
			$userid 					= $this->session->userdata('user_id');
			$quizid 					= $this->uri->segment(3);
			//general_settings
			$quizinfo 					= $this->base_model->run_query(
			"SELECT r.username,r.email,r.score,r.total_questions 
			as maxscore,r.dateoftest,q.name as examname FROM "
			.$this->db->dbprefix('user_quiz_results')." r, "
			.$this->db->dbprefix('quiz')." q WHERE userid=".$userid
			." and quiz_id=".$quizid." and r.quiz_id=q.quizid"
			);
			$quizinfo 					= $quizinfo[0];
			$contentinfo 				= $this->base_model->run_query(
			"select   certificate_logo,certificate_content,
			certificate_sign,certificate_sign_text from "
			.$this->db->dbprefix('general_settings')
			);
			$contentinfo 				= $contentinfo[0];
			$this->data['content'] 		= $contentinfo->certificate_content;
			$this->data['adminsign'] 	= $contentinfo->certificate_sign_text;
			$this->data['signimage'] 	= $contentinfo->certificate_sign;
			$this->data['logo'] 		= $contentinfo->certificate_logo;
			$this->data['content']		= str_replace(
			"__USERNAME__", 
			$quizinfo->username, 
			$this->data['content']
			);
			$this->data['content']		= str_replace(
			"__USERID__",$userid, 
			$this->data['content']
			);
			$this->data['content']		= str_replace(
			"__EMAIL__", 
			$quizinfo->email, 
			$this->data['content']
			);
			$this->data['content']		= str_replace(
			"__COURSENAME__", $quizinfo->examname, 
			$this->data['content']
			);
			$this->data['content']		= str_replace(
			"__SCORE__",
			$quizinfo->score, 
			$this->data['content']
			);
			$this->data['content']		= str_replace(
			"__MAXSCORE__", 
			$quizinfo->maxscore, 
			$this->data['content']
			);
			$this->data['content']		= str_replace(
			"__DATEOFTEST__", 
			$quizinfo->dateoftest, 
			$this->data['content']
			);
			 $html = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 
			 Transitional//EN' ' http://www.w3.org/TR/xhtml1/DTD/xhtml1-
			 transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title>Certificate</title><style>
.certificates {
	width: 680px;
	height: 470px;
	float: left;
	background: url(". base_url()."assets/uploads/certificate/"
	.$this->data['logo'].")
}
.name {
	width: 100%;
	float: left;
	margin-top: 50px;
	text-align: center;
}
.address {
	width: 28%;
	float: right;
	font-family: 'open Sans';
	text-align: left;
	font-size: 13px;
	margin-right: 35px;
	line-height: 20px;
}
.middle-con {
	width: 100%;
	margin: 0px auto;
	margin-top: 100px;
	clear: both;
}
.hed {
	border-bottom: 3px solid;
	font-family: 'open Sans';
	font-size: 17px;
	font-style: italic;
	font-weight: bold;
	margin: 0 auto;
	text-align: center;
	width: 50%;
}
.clear {
	clear: both;
}
.certi-description {
	width: 90%;
	margin: 0px auto;
	clear: both;
	font-family: 'open Sans';
	font-size: 14px;
	text-align: center;
	line-height: 30px;
	text-decoration: underline;
	margin-top: 0px;
	font-style: oblique;
}
.dmmm {
	font-weight: bold;
}
.sgn-ture {
	width: 250px;
	margin: -110px 42px;
	clear: both; float:right;
	 
}

.facualty{ width:250px; float:left; text-align:center; 
 font-family:'open Sans'; font-size:14px; font-weight:bold;}
.director{ width:250px; float:right; text-align:center; 
 font-family:'open Sans'; font-size:14px; font-weight:bold;}
.sign{ float:left; width:250px;}
</style></head>
<body>
<div class='certificates'>
 <br> 
  <div class='middle-con'>
    <div class='hed'> This is to certify that
      <div class='clear'></div>
    </div>
    <div class='clear'></div>
    <div class='certi-description'>".
$this->data['content']
	."</div>
  </div>
  <div class='clear'></div>
  <div class='sgn-ture'>

  <div class='director'><div class='sign'><img src="
  .base_url()."assets/uploads/certificate/".$this->data['signimage']
  ." width='111' height='63' /></div>".$this->data['adminsign']." </div>
  
  <div class='clear'></div>
  </div>
</div>
</body>
</html>";
	$this->data['html'] 				= $html;	

$filename = $userid;
		$pdfFilePath 					= FCPATH."assets/downloads/reports/".$filename.".pdf";

		$data['page_title'] 			= 'Certificate'; // pass data to the view
		if(file_exists($pdfFilePath)){
		 unlink($pdfFilePath); 
		}
		if (file_exists($pdfFilePath) == FALSE) {
		    ini_set('memory_limit','32M'); 
		 $this->load->library('pdf');
		    $pdf 						= $this->pdf->load();
		    $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); 
			$pdf->WriteHTML($html); // write the HTML into the PDF
			$pdf->Output($pdfFilePath, 'F'); // save to file because we can
		}
	redirect("assets/downloads/reports/$filename.pdf"); 
		}
		else {
			 echo "Some problem is their, please contact admin regarding this..";
		}
		$this->load->view('certificate', $this->data);
	}

	
	
 }

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */