<?php defined("BASEPATH") OR exit("No direct script access allowed");

class Home extends CI_Controller {

  function __construct() {
    parent::__construct();
  }

  /**
     * This function is used to load page view
     * @return Void
     */
      public function index(){

    	$this->load->view("front/header");
        $this->load->view("home/index");
        $this->load->view("front/footer");
      }


      public function courses(){

    	$this->load->view("front/header");
        $this->load->view("home/courses");
        $this->load->view("front/footer");
      }

      public function contact(){

    	$this->load->view("front/header");
        $this->load->view("home/contact");
        $this->load->view("front/footer");
      }

      public function singlecourse(){

    	$this->load->view("front/header");
        $this->load->view("home/single-course");
        $this->load->view("front/footer");
      }


}