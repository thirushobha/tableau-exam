    <!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb-area">
        <!-- Breadcumb -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>welcome/courses">Courses</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>welcome/courses/<?php echo $category['catid'];?>"><?php echo $category['name'];?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $subcategory['name'];?></li>
            </ol>
        </nav>
    </div>
    <!-- ##### Breadcumb Area End ##### -->
<!-- style="background-image: url(<?php echo base_url();?>frontassets/img/bg-img/bg3.jpg);" -->
    <!-- ##### Single Course Intro Start ##### -->
    <div class="single-course-intro d-flex align-items-center justify-content-center" style="background-color:  #1d71b9">
        <!-- Content -->
        <div class="single-course-intro-content text-center">
            <!-- Ratings -->
            <!-- <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
            </div> -->
            <h3><?php echo $category['name'];?></h3>
            <div class="meta d-flex align-items-center justify-content-center">
                <a href="#"><?php echo $subcategory['name'];?></a>
                <!-- <span><i class="fa fa-circle" aria-hidden="true"></i></span> -->
                <!-- <a href="#">&#8360; <?php echo $subcategory['amount'];?>/-</a> -->
            </div>
            <div class="price">&#8360; <?php echo $subcategory['amount'];?>/-</div>
        </div>
    </div>
    <!-- ##### Single Course Intro End ##### -->

    <!-- ##### Courses Content Start ##### -->
    <div class="single-course-content section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="course--content">

                        <div class="clever-tabs-content">
                            

                            <div class="tab-content" id="myTabContent">
                                <!-- Tab Text -->
                                <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab--1">
                                    <div class="clever-description">

                                        <!-- About Course -->
                                        <div class="about-course mb-30">
                                            <h4>About this course</h4>
                                            <?php echo $subcategory['description'];?>
                                        </div>

                                        <!-- All Instructors -->
                                        <div class="about-course">
                                            <h4>All Topics</h4>
                                            
                                            <div class="row">
                                                <!-- Single Instructor -->
                                                <?php
                                               if(count($topics)>0){
                                                $i= 1;
                                                                                             
                                               
                                                

                                                foreach ($topics as $key => $value)  { 

                                                    if($i==1){?>

                                                        <?php if( in_array($value['subjectid'],$topics_activate)){
                                                        $disabled = 'bg-warning';
                                                        ?>
                                                        <div class="col-md-3 mb-2 mr-5   text-center <?php echo $disabled;?>">
                                                        <a href= "<?php echo base_url();?>welcome/topics/<?php echo $category['catid'];?>/<?php echo $subcategory['subcatid'];?>/<?php echo $value['subjectid'];?>"  class="   text-center " disabled="true" style="width: 100%;"><?php echo $value['name'];?></a>
                                                        </div>

                                                   <?php }else{
                                                        $disabled  ='bg-secondary';

                                                        ?>
                                                        <div class="col-md-3 mb-2 mr-5   text-center <?php echo $disabled;?>">
                                                        <a  href="#" class="text-center " disabled="true" style="width: 100%;" data-toggle="modal" data-target="#myModal"><?php echo $value['name'];?></a>
                                                        </div>
                                                        
                                                   <?php }
                                                    ?>



                                                    <?php }else{
                                                        if($enrolled_course==TRUE || $enrolled_entire_course==TRUE){ ?>


                                                            <?php if( in_array($value['subjectid'],$topics_activate)){
                                                        $disabled = 'bg-warning';
                                                        ?>
                                                        <div class="col-md-3 mb-2 mr-5   text-center <?php echo $disabled;?>">
                                                        <a href= "<?php echo base_url();?>welcome/topics/<?php echo $category['catid'];?>/<?php echo $subcategory['subcatid'];?>/<?php echo $value['subjectid'];?>"  class="   text-center " disabled="true" style="width: 100%;"><?php echo $value['name'];?></a>
                                                        </div>

                                                   <?php }else{
                                                        $disabled  ='bg-secondary';

                                                        ?>
                                                        <div class="col-md-3 mb-2 mr-5   text-center <?php echo $disabled;?>">
                                                        <a  href="#" class="text-center " disabled="true" style="width: 100%;" data-toggle="modal" data-target="#myModal"><?php echo $value['name'];?></a>
                                                        </div>
                                                        
                                                   <?php }
                                                    ?>



                                                        <?php }else{
                                                             $disabled  ='bg-secondary';
                                                            ?>
                                                            <div class="col-md-3 mb-2 mr-5   text-center <?php echo $disabled;?>">
                                                        <a  href="#" class="text-center " disabled="true" style="width: 100%;" data-toggle="modal" data-target="#myModal1"><?php echo $value['name'];?></a>
                                                        </div>

                                                        <?php
                                                    }


                                                    }
                                                    ?>

                                                    
                                                    
                                                
                                                        <!-- <div class="col-md-3 mb-2 mr-5   text-center <?php echo $disabled;?>">
                                                        <a href= "<?php echo base_url();?>welcome/topics/<?php echo $category['catid'];?>/<?php echo $subcategory['subcatid'];?>/<?php echo $value['subjectid'];?>"  class="   text-center " disabled="true" style="width: 100%;"><?php echo $value['name'];?></a>
                                                        </div> -->
                                                <?php  
                                                $i++;

                                            } 
                                        }else{
                                            echo "We Will Update Topics Soon";
                                        } ?>


                                               
                                            </div>
                                        </div>



































                                        <!-- All Instructors -->
                                        <div class="about-course">
                                            <h4>Final Tests For Certification</h4>
                                            
                                            <div class="row">
                                                <?php
                                                if($display_final_quizzes)
                                                {
                                                    $i = 1;
                                                    // echo "You have access to Continue";
                                                    foreach($final_quizzes as $fq)
                                                    {
                                                        

                                                 if($i==1){
                                                        // echo $fq['name'];
                                                        ?>
                                                        <a href="<?php echo base_url()?>user/instructions1/<?php echo $fq['quizid'];?>/<?php echo $fq['name'];?>" class="btn bg-primary wnm-user mr-5" target="_blank"> <i class="fa fa-puzzle-piece"></i> Final Quiz <?php echo $i;?></a>


                                                    <?php
                                                }else
                                                {
                                                    $previous_index = $i-1;
                                                    $column_name = 'quiz'.$previous_index.'_status';
                                                    if($final_quizzes_finished[$column_name]==1)
                                                    {
                                                        ?>
                                                        <a href="<?php echo base_url()?>user/instructions1/<?php echo $fq['quizid'];?>/<?php echo $fq['name'];?>" class="btn bg-primary wnm-user mr-5" target="_blank"> <i class="fa fa-puzzle-piece"></i> Final Quiz <?php echo $i;?></a>
    
                                                    <?php
                                                }else{

                                                   ?>
                                                   <a href="#" class="btn bg-secondary  mr-5 " disabled="true"  data-toggle="modal" data-target="#myModal2"> <i class="fa fa-puzzle-piece"></i> Final Quiz <?php echo $i;?></a>





                                                   <?php
                                               }
                                                }

                                                    $i++;
                                                }
                                                }else{
                                                    echo "Finish All Topics To activate this panel";
                                                }

                                                ?>
                                               
                                                                
    
                                               
                                            </div>
                                        </div>


























                                       
                                    </div>
                                </div>

                                
                              
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-4">
                    <div class="course-sidebar">
                        <!-- Buy Course -->
                        <!-- <a href="#" class="btn clever-btn mb-30 w-100">Buy course</a> -->
                        <?php
                    if($category['amount']!=''){
                    if($enrolled_course==TRUE || $enrolled_entire_course==TRUE){

                        ?>
                        <a href="#" class="btn btn-success mb-30 w-100">Already Purchased</a>

                        <?php
                    }else{
                    ?>

                        <a href="<?php echo base_url();?>instamojo_payment/index/<?php echo $subcategory['amount'];?>/<?php echo $category['catid'];?>/<?php echo $subcategory['subcatid'];?>" class="btn clever-btn mb-30 w-100">Buy This Course @ &#8360;<?php echo $subcategory['amount'];?>/- </a>
                        

                    <?php
                    }
                    }
                    ?>


                    <?php
                    if($final_quizzes_finished['entire_status']==1)
                    {
                        echo '<a href="'.base_url().'welcome/certificate/'.$final_quizzes[0]['quizid'].'" class="btn btn-success mb-30 w-100" target="_blank">Download Certificate</a>';
                    }else{
                        ?>
                        <a href="#" class="btn bg-secondary w-100 " disabled="true"  data-toggle="modal" data-target="#myModal3"> <i class="fa fa-puzzle-piece"></i> Download Certificate</a>

                    <?php
                }



                    ?>





                        <!-- Widget -->
                        <div class="sidebar-widget">
                            <h4>Course Features</h4>
                            <ul class="features-list">
                                <li>
                                    <h6><i class="fa fa-clock-o" aria-hidden="true"></i> Duration</h6>
                                    <h6>Life Time</h6>
                                </li>
                                <li>
                                    <h6><i class="fa fa-bell" aria-hidden="true"></i> Lectures</h6>
                                    <h6><?php echo $lectures_count;?></h6>
                                </li>
                                <li>
                                    <h6><i class="fa fa-file" aria-hidden="true"></i> Quizzes</h6>
                                    <h6><?php echo $quizzes_count;?></h6>
                                </li>
                                <li>
                                    <h6><i class="fa fa-thumbs-up" aria-hidden="true"></i> Pass Percentage</h6>
                                    <h6>60</h6>
                                </li>
                                <li>
                                    <h6><i class="fa fa-thumbs-down" aria-hidden="true"></i> Max Quiz Retakes</h6>
                                    <h6>Unlimited</h6>
                                </li>
                            </ul>
                        </div>

                        <!-- Widget -->
                        <div class="sidebar-widget">
                            <h4>Certification</h4>
                            <img src="<?php echo base_url();?>frontassets/img/bg-img/cer.png" alt="">
                        </div>

                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Courses Content End ##### -->

    <!-- Button to Open the Modal -->
  

  <!-- The Modal -->
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Alert!...</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          Please Finish Before Topics To Activate This topic
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>

  <!-- The Modal -->
  <div class="modal" id="myModal1">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Alert!...</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          Please Purchase Course To Continue
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>




  <!-- The Modal -->
  <div class="modal" id="myModal2">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Alert!...</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          Please Finish Before Exam to Activate This
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>


    <!-- The Modal -->
  <div class="modal" id="myModal3">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Alert!...</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          Please Finish All Final Quizzes ...!
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>

   
