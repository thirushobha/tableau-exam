    <!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb-area">
        <!-- Breadcumb -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>welcome/courses">Courses</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $category['name'];?></li>
            </ol>
        </nav>
    </div>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Catagory ##### -->
    <!--  -->
    <div class="clever-catagory bg-img d-flex align-items-center justify-content-center p-3" style="background-image: url(img/bg-img/bg2.jpg);">
        <h3><?php echo $category['name'];?></h3>
    </div>

<?php
if($category['amount']>0){
    if($enrolled_entire_course==TRUE){

        ?>

<section class="popular-courses-area pt-5">
    <div class="container">
    <h3>You Enroleld This Cource Please Start to Learn</h3>
    
    </div>
</section>

    <?php
}else{
?>
<section class="popular-courses-area pt-5">
    <div class="container">
    <h3>If You Want To Purchase Entire Package Price: <strong class="text-danger"> &#8360; .<?php echo $category['amount'];?></strong></h3>
    <a href="<?php echo base_url();?>instamojo_payment/index/<?php echo $category['amount'];?>/<?php echo $category['catid'];?>" class="btn btn-primary">Buy This Course</a>
    </div>
</section>

<?php
}
}
?>
    <!-- ##### Popular Course Area Start ##### -->
    <section class="popular-courses-area section-padding-100">
        <div class="container">
            <div class="row">

                <?php
                if(count($subcategories)>0){
                    foreach ($subcategories as $subcategory) {
                     
                ?>
                <!-- Single Popular Course -->
                <div class="col-12 col-md-6 col-lg-4">
                    <a href="<?php echo base_url();?>welcome/singlecourse/<?php echo $category['catid'];?>/<?php echo $subcategory['subcatid'];?>">
                    <div class="single-popular-course mb-100 wow fadeInUp" data-wow-delay="250ms">
                        <!-- <img src="img/bg-img/c1.jpg" alt=""> -->
                        <!-- Course Content -->
                        <div class="course-content">
                            <h4><?php echo $subcategory['name'];?></h4>
                            <div class="meta d-flex align-items-center">
                                <!-- <a href="#">Sarah Parker</a>
                                <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                                <a href="#">Art &amp; Design</a> -->
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce enim nulla, mollis eu metus in, sagittis</p>
                        </div>
                        <!-- Seat Rating Fee -->
                       <!--  <div class="seat-rating-fee d-flex justify-content-between">
                            <div class="seat-rating h-100 d-flex align-items-center">
                                <div class="seat">
                                    <i class="fa fa-user" aria-hidden="true"></i> 10
                                </div>
                                <div class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i> 4.5
                                </div>
                            </div>
                            <div class="course-fee h-100">
                                <a href="#" class="free">Free</a>
                            </div>
                        </div> -->
                    </div>
                    </a>
                </div>

                <?php
            }

                    }

                ?>

               
              
            </div>

           <!--  <div class="row">
                <div class="col-12">
                    <div class="load-more text-center wow fadeInUp" data-wow-delay="1000ms">
                        <a href="#" class="btn clever-btn btn-2">Load More</a>
                    </div>
                </div>
            </div> -->
        </div>
    </section>
    <!-- ##### Popular Course Area End ##### -->
