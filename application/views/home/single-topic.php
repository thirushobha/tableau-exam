    <!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb-area">
        <!-- Breadcumb -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>welcome/courses">Courses</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>welcome/courses/<?php echo $category['catid'];?>"><?php echo $category['name'];?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $subcategory['name'];?></li>
            </ol>
        </nav>
    </div>
    <!-- ##### Breadcumb Area End ##### -->
    <style type="text/css">
        .single-course-intro{
            height: 500px;
        }
    </style>

    <!-- ##### Single Course Intro Start ##### -->
    <div class="single-course-intro d-flex align-items-center justify-content-center" >
       <div style="text-align:center"> 
         
         <!--  <button onclick="makeBig()">Big</button>
          <button onclick="makeSmall()">Small</button>
          <button onclick="makeNormal()">Normal</button> -->
          <br><br>
          <?php
          if($subjects['is_uploaded']==1){
          ?>
          <video id="video1" width="800px" controlsList="nodownload" disablepictureinpicture >
            <source src="<?php echo base_url().$subjects['video_url'];?>" type="video/mp4">
              <source src="<?php echo base_url().$subjects['video_url'];?>" type="video/ogg">
                <source src="<?php echo base_url().$subjects['video_url'];?>" type="video/webm">
                 
            
            Your browser does not support HTML5 video.
          </video>
          <br>
           <button onclick="playPause()" class="btn btn-primary">Play/Pause</button> 
          <a href="#" id="fullscreen" class="btn btn-warning">Go Fullscreen</a>
      <?php }else{ ?>
<h3>Sorry No Video Uploaded, Coming Soon</h3>
      <?php } ?>

          
          
        </div> 

        <!-- Content -->
       <!--  <div class="single-course-intro-content text-center">
            
            <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
            </div>
            <h3>English Grammar</h3>
            <div class="meta d-flex align-items-center justify-content-center">
                <a href="#">Sarah Parker</a>
                <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                <a href="#">Art &amp; Design</a>
            </div>
            <div class="price">Free</div>
        </div> -->
    </div>
    <!-- ##### Single Course Intro End ##### -->

      <!-- The Modal -->
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Test Access Denied</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          You watch Full Video Then You will Write Exam .
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>

  <!-- The Modal -->
  <div class="modal" id="myModal1">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Test Access Denied</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          You Take Exam and Pass With 60% Then this tab visible .
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>



    <!-- ##### Courses Content Start ##### -->
    <div class="single-course-content section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-12">
                    <div class="course--content">

                        <div class="clever-tabs-content">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="tab--1" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="false">Description</a>
                                </li>
                                <li class="nav-item">
                                    <?php
                                    if($topics_activate['video_status']==1){ ?>
                                        <a  id="tab--2" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="true" class="nav-link">Test</a>

                                    <?php }else{ ?>
                                        <button type="button" class="nav-link" data-toggle="modal" data-target="#myModal">
                                       Test
                                      </button>

                                    <?php }

                                    ?>
        
                                    
                                    
                                    
                                </li>
                                <li class="nav-item">

                                  <?php
                                    if($topics_activate['quiz_status']==1){ ?>
                                       <a class="nav-link" id="tab--3" data-toggle="tab" href="#tab3" role="tab" aria-controls="tab3" aria-selected="true">Result</a>

                                    <?php }else{ ?>
                                        <button type="button" class="nav-link" data-toggle="modal" data-target="#myModal1">
                                       Result
                                      </button>

                                    <?php }

                                    ?>


                                    
                                </li>

                                <li class="nav-item">

                                 
                                       <a class="nav-link" id="tab--4" data-toggle="tab" href="#tab4" role="tab" aria-controls="tab4" aria-selected="true">Sample Data</a>

                                    


                                    
                                </li>

                                
                                
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                <!-- Tab Text -->
                                <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab--1">
                                    <div class="clever-description">

                                        <!-- About Course -->
                                        <div class="about-course mb-30">
                                            <h4>About this Topic</h4>
                                            <?php echo $subjects['description'];?>
                                        </div>

                                        
                                    </div>
                                </div>

                                

                                <!-- Tab Text -->
                                <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab--2">
                                    <div class="clever-curriculum">

                                        <!-- About Curriculum -->
                                        <div class="about-curriculum mb-30">
                                            <h4>Test</h4>
                                            <div class="row">
                                                 <div class="col-md-12">
                                                    <table id="example" class="table table-bordered table-condensed " >
                                                       <thead>
                                                          <tr>
                                                             <th>S.No.</th>
                                                             <th>Quiz Name</th>
                                                             <th>Category</th>
                                                             <th>Sub Category</th>
                                                             
                                                             <th>Duration</th>
                                                             <th>Action</th>
                                                          </tr>
                                                       </thead>
                                                       
                                                       <tbody>

                                                       </tbody>
                                                    </table>
                                                 </div>
                                              </div>
                                        </div>

                                       
                                    </div>
                                </div>

                                <!-- Tab Text -->
                                <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab--3">
                                    <div class="clever-review">

                                        <!-- About Review -->
                                        <div class="about-review mb-30">
                                            <h4>Reviews</h4>
                                            <p>Sed elementum lacus a risus luctus suscipit. Aenean sollicitudin sapien neque, in fermentum lorem dignissim a. Nullam eu mattis quam. Donec porttitor nunc a diam molestie blandit. Maecenas quis ultrices</p>
                                        </div>

                                        
                                    </div>
                                </div>

                                <!-- Tab Text -->
                                <div class="tab-pane fade" id="tab4" role="tabpanel" aria-labelledby="tab--4">
                                    <div class="clever-review">

                                        <!-- About Review -->
                                        <div class="about-review mb-30">
                                            <h4>Sample Data File Download link Here</h4>
                                            <a href="#" class="btn btn-warning">Download <i class="fa fa-download" aria-hidden="true"></i>
                                                </a>
                                        </div>

                                        
                                    </div>
                                </div>

                               

                               

                               
                            </div>
                        </div>
                    </div>
                </div>

               
            </div>
        </div>
    </div>
    <!-- ##### Courses Content End ##### -->
<script> 
var myVideo = document.getElementById("video1"); 

function playPause() { 
  if (myVideo.paused) 
    myVideo.play(); 
  else 
    myVideo.pause(); 
} 

function makeBig() { 
    myVideo.width = 560; 
} 

function makeSmall() { 
    myVideo.width = 320; 
} 

function makeNormal() { 
    myVideo.width = 420; 
} 
</script>

<?php
$catid = $this->uri->segment(3);
$subcatid = $this->uri->segment(4);
$subjectid = $this->uri->segment(5);
?>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets/designs/js/jquery.dataTables.js"></script>
<script type='text/javascript'>
    document.getElementById('video1').addEventListener('ended',myHandler,false);
    function myHandler(e) {
        // What you want to do after the event
        var catid1  = "<?php echo $catid;?>";
    var subcatid1  = '<?php echo $subcatid;?>';
    var subjectid1  = '<?php echo $subjectid;?>';
    
        $.ajax({
  
        type:'POST',
        url:'<?php echo base_url();?>welcome/videostatus_update',
        data:'catid='+catid1+'&subcatid='+subcatid1+'&subjectid='+subjectid1+'&<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>',
        cache:false,
        success: function(data) {
        //alert(data);
            var dta = $.parseJSON(data);
            if(!alert(dta['msg'])){window.location.reload();}
                  
        }
    });
    }



$(document).ready(function() {
    $('#example').dataTable();
      var catid  = "<?php echo $catid;?>";
    var subcatid  = '<?php echo $subcatid;?>';
    var quiztype  = '';
    var difficultylevel  = '';
    var subjectid = "<?php echo $subjectid;?>";

    
   $.ajax({
  
    
        type:'POST',
        url:'<?php echo base_url();?>user/get_quizzes',
        data:'catid='+catid+'&subcatid='+subcatid+'&subjectid='+subjectid+'&quiztype='+quiztype+'&difficultylevel='+difficultylevel+'&<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>',
        cache:false,
        success: function(data) {
        //alert(data);
            var dta = $.parseJSON(data);
            if(dta.length>0)
            {
                var data_to_append = '';
                
                var thead = '<thead><tr><th>S.No.</th><th>Quiz Name</th><th>Category</th><th>Sub Category</th><th>Duration</th><th>Action</th></tr></thead> ';
                
                var tfoot = '<tfoot><tr><th>S.No.</th><th>Quiz Name</th><th>Category</th><th>Sub Category</th><th>Duration</th><th>Action</th></tr></tfoot>';
                
                var tbody_open = '<tbody>';
                var tbody_content = '';
                var tbody_close = '</tbody>';                       
                
                var j=1;
                for(i=0; i<dta.length; i++)
                {
                    //var start_date = get_date_format(dta[i].startdate);
                    //var end_date = get_date_format(dta[i].enddate);
                    var quiz = dta[i].quizid;
                    
                    //Get Subjects according to the Quiz
                    //var subjectsTd = get_subjects(quiz);
                    
                    // tbody_content = tbody_content + '<tr><td>'+j+'</td><td>'+dta[i].name+'</td><td>'+dta[i].catname+'</td><td>'+dta[i].subcatname+'</td><td>'+dta[i].difficultylevel+'</td><td>'+dta[i].quiztype+'</td><td>'+dta[i].quizcost+'</td><td>'+dta[i].validityvalue+'</td><td>'+dta[i].deauration+'</td><td><a href="<?php echo base_url()?>user/instructions1/'+dta[i].quizid+'/'+dta[i].name+'" class="btn bg-primary wnm-user"> <i class="fa fa-puzzle-piece"></i> Take Exam</a></td></tr>';
                    tbody_content = tbody_content + '<tr><td>'+j+'</td><td>'+dta[i].name+'</td><td>'+dta[i].catname+'</td><td>'+dta[i].subcatname+'</td><td>'+dta[i].deauration+'</td><td><a href="<?php echo base_url()?>user/instructions1/'+dta[i].quizid+'/'+dta[i].name+'" class="btn bg-primary wnm-user"> <i class="fa fa-puzzle-piece"></i> Take Exam</a></td></tr>';
                    j++;
                }
                
                data_to_append = data_to_append + tbody_open + tbody_content + tbody_close;
                //alert(data_to_append);                        
                
                $('#example').empty();                      
                $('#example').dataTable().fnDestroy();
                $('#example').append(data_to_append);
                $('#example').dataTable();
                
               
                
            }
            else
            {
                $('#example').empty();                      
                $('#example').dataTable().fnDestroy();
                $('#example').dataTable({"oLanguage": {"sEmptyTable": "No Matching Records Found."}});
                
                                      
                
            }       
        }
    });

   var element = document.getElementById('video1');
            var fullscreen = document.getElementById('fullscreen');

            fullscreen.addEventListener('click',function(){
                <!--console.log ('it is working'); -->
                if(element.requestFullscreen){
                    element.requestFullscreen();
                } 
                else if (element.webkitRequestFullscreen){
                    element.webkitRequestFullscreen();
                }
                else if (element.mozRequestFullScreen){
                    element.mozRequestFullScreen();
                }
                else if (element.msRequestFullscreen){
                    element.msRequestFullscreen();
                }   
            });

            var supposedCurrentTime = 0;
myVideo.addEventListener('timeupdate', function() {
  if (!myVideo.seeking) {
        supposedCurrentTime = myVideo.currentTime;
  }
});

            // prevent user from seeking
myVideo.addEventListener('seeking', function() {
  // guard agains infinite recursion:
  // user seeks, seeking is fired, currentTime is modified, seeking is fired, current time is modified, ....
  var delta = myVideo.currentTime - supposedCurrentTime;
  if (Math.abs(delta) > 0.01) {
    console.log("Seeking is disabled");
    myVideo.currentTime = supposedCurrentTime;
  }
});



});
</script>