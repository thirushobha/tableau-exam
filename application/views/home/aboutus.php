<!-- Slider-->
<div data-ride="carousel" class="carousel slide banner" id="myCarousel">
   <div class="container-fluid padding">
      <img src="<?php echo base_url(); ?>assets/designs/images/inner-banner.png" width="100%" height="150px">
   </div>
</div>
<!-- Slider-->
<!-- Middle Content-->
<div class="container-fluid content-bg">
   <div class="spacer"></div>
   
   <div class="container inner-content padding">
      <div class="col-md-8 col-xs-12">
         <h1 class="inner-hed">About Us</h1>
         <p>
            <?php if (isset($aboutus_content) && count($aboutus_content) > 0) { 
               foreach ($aboutus_content as $c) {
				echo $c->content;
            ?> 
            <?php } 
			} 
			else echo "Coming Soon.";
			?>
         </p>
      </div>
     
   </div>
   <div class="spacer"></div>
</div>
</div>
<!-- Middle Content-->

