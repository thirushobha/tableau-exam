<!-- Bootstrap -->
<link href="<?php echo base_url();?>assets/designs/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/designs/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/designs/css/style.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/designs/font.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/designs/css/editor.css" type="text/css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/designs/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/designs/js/editor.js"></script>
<script src="<?php echo base_url();?>assets/designs/js/bootstrap.min.js"></script>
<script type="text/javascript">
   $(document).ready( function() {
          $("#txtEditor").Editor();
  
  //Get Sub Categories on change of Categories
    $('#catid').change(function() {
      var catid=$(this).val();
      if(catid=="") {
         $('#subcatid').empty();
         $('#subcatid').append("<option value=''>Select Category First.</option>");
      }
   
      $.ajax({
      
         type:'POST',
         url:'<?php echo base_url();?>admin/getSubCategories',
         data:'catid='+catid+'&<?php echo $this->security->get_csrf_token_name();?>=<?php echo $this->security->get_csrf_hash();?>',
         cache:false,
         success: function(data) {
         
            var dta = $.parseJSON(data);
            if (dta.length>0) {
               $('#subcatid').empty();
               $('#subcatid').append("<option value=''>Select Sub Category</option>");
               for(i=0; i<dta.length; i++)
               $('#subcatid').append("<option value='"+dta[i].subcatid+"'>"+dta[i].name+"</option>");
            }
            else {
               $('#subcatid').empty();
               alert("No sub categories Available");  
            }     
         }
      });
    });

  });

    </script>
<div class="col-md-12 padd">
   <div class="bradcome-menu">
      <ul>
         <li><a href="<?php echo base_url();?>admin">Home</a></li>
         <li><img  src="<?php echo base_url();?>assets/designs/images/arrow.png"></li>
         <li><a href="<?php echo base_url();?>admin/subjects"> Topics</a></li>
         <li><img  src="<?php echo base_url();?>assets/designs/images/arrow.png"></li>
         <li><a href="#"> <?php if(count($data)>0) echo "Update Topic"; else echo "Add Topic";?></a></li>
      </ul>
   </div>
</div>
<div class="row">
   <div class="col-md-8">
      <div style="color:#FF0000; font-size:12px; padding-left:10px;"><?php 
         echo $this->session->flashdata('message');
         ?>
      </div>
      <form method="POST" action="<?php echo base_url();?>admin/addeditSubjects" id="subjects_form" enctype="multipart/form-data">
         <?php if(count($data)>0)
            $data=$data[0];
            ?> 

		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

    <div class="form-group">
            <label for="inputEmail">Category</label>
            <?php 
               $val = '';
                  if ($this->input->post( 'catid' )) {
               $val = $this->input->post( 'catid' );
                  }
                  elseif (count($data)) {
                     $val = $data->catid;
                  }
               echo form_dropdown('catid', $categories, $val,'class="form-control" id="catid"');
               ?>
         </div>
         <div class="form-group">
            <label for="inputEmail">Sub Category</label>
            <?php 
               $val = '';
               $name = 'Select Category First.';
                  if ($this->input->post( 'subcatid' )) {
               $val = $this->input->post('subcatid');
               $name = '';
                  }
                  elseif (count($data)) {
                     $val = $data->subcatid;
                     $name = $data->subcatname;
                  }
               ?>
            <select name="subcatid" id="subcatid" class="form-control">
               <option value="<?php echo $val;?>"><?php echo $name;?></option>
            </select>
         </div>
			
         <div class="form-group">
            <label for="inputEmail">Topic Name</label>
            <?php 
               $val = '';
               	if ($this->input->post('name')) {
					$val = $this->input->post( 'name' );
               	}
               	elseif (count($data)) {
               		$val = $data->name;
               	}
			?>
            <input type="text" class="form-control" id="name" name="name" value="<?php echo $val;?>" placeholder="Enter Subject Name" >
         </div>
          <div class="form-group">
            <label for="inputEmail">Description</label>
      
      <?php 
        $val = '';
          if( ( $this->input->post( 'content' ) ) )
          {
          $val = $this->input->post( 'content' );
          }
          elseif(count($data))
          {
            $val = $data->description;
          }
  
      ?>
            
            <textarea class="editors" id="editor1" name="content" placeholder="Type Content for Aboutus Page" ><?php echo $val;?></textarea>
        </div>

        <div class="form-group">
            <label for="inputEmail">Youtube Video Link</label>
            <?php 
               $val = '';
                if ($this->input->post('youtube_url')) {
          $val = $this->input->post( 'youtube_url' );
                }
                elseif (count($data)) {
                  $val = $data->youtube_url;
                }
      ?>
            <input type="text" class="form-control" id="youtube_url" name="youtube_url" value="<?php echo $val;?>" placeholder="Enter Youtube Link" >
         </div>



        <div class="form-group">
            <label for="inputEmail">Upload Video</label>
            <?php 
               $val = '';
                if ($this->input->post('video_url')) {
          $val = $this->input->post( 'video_url' );
                }
                elseif (count($data)) {
                  $val = $data->video_url;
                }
      ?>
            <input type="file" class="form-control" id="name" name="video_url" value="<?php echo $val;?>"  >
         </div>

         <div class="form-group">
            <label for="inputEmail">Upload Sample File</label>
            <?php 
               $val = '';
                if ($this->input->post('sample_url')) {
          $val = $this->input->post( 'sample_url' );
                }
                elseif (count($data)) {
                  $val = $data->sample_url;
                }
      ?>
            <input type="file" class="form-control" id="name" name="sample_url" value="<?php echo $val;?>"  >
         </div>



         <div class="form-group">
            <label for="inputEmail">Status</label>
            <?php 
               $val = '';
               	if ($this->input->post('status')) {
					$val = $this->input->post('status');
               	}
               	elseif (count($data)) {
               		$val = $data->status;
               	}
               echo form_dropdown('status', $element, $val,'class="form-control"');
               ?>
         </div>
         <input type="hidden" name="id" value="<?php if(isset($id)) echo $id;?>">
         <button type="submit" class="btn btn-primary wnm-user"><?php if(count($data)>0) echo "Update"; else echo "Add";?></button>
      </form>
   </div>
</div>
<!-- Validations -->
<script src="<?php echo base_url();?>assets/designs/js/jquery.validate.min.js"></script>
<script type="text/javascript"> 
   (function($,W,D)
   {
      var JQUERY4U = {};
   
      JQUERY4U.UTIL =
      {
          setupFormValidation: function()
          {		
   		//form validation rules
              $("#subjects_form").validate({
   			rules: {
   				name: {
   					required: true,
   					rangelength: [2, 30]
   				},
          catid:{
            required:true,
          },
          subcatid:{
            required:true,
          }
   				
                  },
   			
   			messages: {
   				name: {
                          required: "Please enter subject name."
                      }
   			},
                  
                  submitHandler: function(form) {
                      form.submit();
                  }
              });
          }
      }
   
      //when the dom has loaded setup form validation rules
      $(D).ready(function($) {
          JQUERY4U.UTIL.setupFormValidation();
      });
   
   })(jQuery, window, document);
   
</script>
 <!-- CK EDITOR -->
     <script src="<?php echo base_url();?>assets/designs/ckeditor.js"></script>
    <script>

        $(function() {
          $('.editors').each(function(){
          
            CKEDITOR.replace($(this).attr('id'), { 
          /*
           * Ensure that htmlwriter plugin, which is required for this sample, is loaded.
           */
          extraPlugins: 'htmlwriter',
          
          /*
           * Style sheet for the contents
           */
          contentsCss: 'body {color:#000; background-color#:FFF;}',

          /*
           * Simple HTML5 doctype
           */
          docType: '<!DOCTYPE HTML>',

          /*
           * Allowed content rules which beside limiting allowed HTML
           * will also take care of transforming styles to attributes
           * (currently only for img - see transformation rules defined below).
           *
           * Read more: http://docs.ckeditor.com/#!/guide/dev_advanced_content_filter
           */
          allowedContent:
            'h1 h2 h3 p pre[align]; ' +
            'blockquote code kbd samp var del ins cite q b i u strike ul ol li hr table tbody tr td th caption; ' +
            'img[!src,alt,align,width,height]; font[!face]; font[!family]; font[!color]; font[!size]; font{!background-color}; a[!href]; a[!name]',

          /*
           * Core styles.
           */
          coreStyles_bold: { element: 'b' },
          coreStyles_italic: { element: 'i' },
          coreStyles_underline: { element: 'u' },
          coreStyles_strike: { element: 'strike' },

          /*
           * Font face.
           */

          // Define the way font elements will be applied to the document.
          // The "font" element will be used.
          font_style: {
            element: 'font',
            attributes: { 'face': '#(family)' }
          },

          /*
           * Font sizes.
           */
          fontSize_sizes: 'xx-small/1;x-small/2;small/3;medium/4;large/5;x-large/6;xx-large/7',
          fontSize_style: {
            element: 'font',
            attributes: { 'size': '#(size)' }
          },

          /*
           * Font colors.
           */

          colorButton_foreStyle: {
            element: 'font',
            attributes: { 'color': '#(color)' }
          },

          colorButton_backStyle: {
            element: 'font',
            styles: { 'background-color': '#(color)' }
          },

          /*
           * Styles combo.
           */
          stylesSet: [
            { name: 'Computer Code', element: 'code' },
            { name: 'Keyboard Phrase', element: 'kbd' },
            { name: 'Sample Text', element: 'samp' },
            { name: 'Variable', element: 'var' },
            { name: 'Deleted Text', element: 'del' },
            { name: 'Inserted Text', element: 'ins' },
            { name: 'Cited Work', element: 'cite' },
            { name: 'Inline Quotation', element: 'q' }
          ],

          on: {
            pluginsLoaded: configureTransformations,
            loaded: configureHtmlWriter
          }
        });
      });
      
      
      
    });

        /*
         * Add missing content transformations.
         */
        function configureTransformations( evt ) {
          var editor = evt.editor;

          editor.dataProcessor.htmlFilter.addRules( {
            attributes: {
              style: function( value, element ) {
                // Return #RGB for background and border colors
                return CKEDITOR.tools.convertRgbToHex( value );
              }
            }
          } );

          // Default automatic content transformations do not yet take care of
          // align attributes on blocks, so we need to add our own transformation rules.
          function alignToAttribute( element ) {
            if ( element.styles[ 'text-align' ] ) {
              element.attributes.align = element.styles[ 'text-align' ];
              delete element.styles[ 'text-align' ];
            }
          }
          editor.filter.addTransformations( [
            [ { element: 'p', right: alignToAttribute } ],
            [ { element: 'h1',  right: alignToAttribute } ],
            [ { element: 'h2',  right: alignToAttribute } ],
            [ { element: 'h3',  right: alignToAttribute } ],
            [ { element: 'pre', right: alignToAttribute } ]
          ] );
        }

        /*
         * Adjust the behavior of htmlWriter to make it output HTML like FCKeditor.
         */
        function configureHtmlWriter( evt ) {
          var editor = evt.editor,
            dataProcessor = editor.dataProcessor;

          // Out self closing tags the HTML4 way, like <br>.
          dataProcessor.writer.selfClosingEnd = '>';

          // Make output formatting behave similar to FCKeditor.
          var dtd = CKEDITOR.dtd;
          for ( var e in CKEDITOR.tools.extend( {}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent ) ) {
            dataProcessor.writer.setRules( e, {
              indent: true,
              breakBeforeOpen: true,
              breakAfterOpen: false,
              breakBeforeClose: !dtd[ e ][ '#' ],
              breakAfterClose: true
            });
          }
        }

      </script> 
