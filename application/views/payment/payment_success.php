

    <!-- ##### Contact Area Start ##### -->
    <section class="contact-area">
        <div class="container">
            <div class="row">
                <!-- Contact Info -->
                <div class="col-12 col-lg-6">
                    <div class="contact--info mt-50 mb-100">
                        <h4>Payment Success</h4>
                        <p>You Enrolled this cource to Continue</p>

                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    Name
                                </td>
                                <td>
                                    <?php echo $user_details->username;?>
                                </td>
                            </tr>
                            <tr>
                                <td>Course Name:</td>
                                <td><?php echo $category['name'];?></td>
                            </tr>
                            <?php
                            if($subcategory!=''){
                            ?>
                             <tr>
                                <td>Subcategory Name:</td>
                                <td><?php echo $subcategory['name'];?></td>
                            </tr>
    
                            <?php 
                        }
                            ?>

                            <tr>
                                <td>Start The Course</td>
                                <td><a href="<?php echo base_url();?>welcome/courses/<?php echo $category['catid'];?>" class="btn btn-warning">Start Here</a></td>
                            </tr>

                        </table>

                        
                    </div>
                </div>

               
            </div>
        </div>
    </section>
    <!-- ##### Contact Area End ##### -->
