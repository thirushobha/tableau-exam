 <!-- Slider-->
<div data-ride="carousel" class="carousel slide banner" id="myCarousel">
   <div class="container-fluid padding">
      <img src="<?php echo base_url(); ?>assets/designs/images/password_banner.jpg" width="100%" height="150px">
   </div>
</div>
<!-- Slider-->
 <!-- ##### Contact Area Start ##### -->
    <section class="contact-area">
        <div class="container">
            <div class="row">
                

                <!-- Contact Form -->
                <div class="col-12 col-lg-6">
                    <div class="contact-form">
                        <h4>Forgot Password</h4>
                        
                        <div id="infoMessage"><?php  echo $message;?></div>
            <?php echo form_open("auth/forgot_password",'class="form-signin" id="forgot_password_form"');?>
            <div class="form-group paddin-cont">
               <label class="col-lg-3 control-label" for="ftname">Email <span style="color:red;">*</span></label>
               <div class="col-lg-9 ">
                  <?php echo form_input($email);?>
               </div>
            </div>
            <div style="margin-left:60px;" class="form-group ">
               <div class="col-lg-offset-1 col-lg-10 padd">
                  <?php echo form_submit('submit', lang('forgot_password_submit_btn'),'class="btn btn-danger butt"');?>
               </div>
            </div>
            </form>

            <div class="text-center">
               <ul class="">
                  <li>Do you Already have account: <a class=" btn btn-info" href="<?php echo base_url(); ?>auth/login"><?php echo lang('login_submit_btn'); ?></a></li><br>
                  <li> Do you want to create account: <a href="<?php echo base_url(); ?>auth/create_user" class=" btn btn-warning"> <?php echo lang('signup_user_submit_btn'); ?></a></li>
               </ul>
            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Contact Area End ##### -->

<!-- Validations -->
<script src="<?php echo base_url();?>assets/designs/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/designs/js/jquery.validate.min.js"></script>
<script type="text/javascript"> 
   (function($,W,D)
   {
      var JQUERY4U = {};
   
      JQUERY4U.UTIL =
      {
          setupFormValidation: function()
          {	
   		
   		//form validation rules
              $("#forgot_password_form").validate({
                  rules: {
                      "email": {
                          required: true,
   					email: true
                      }
                  },
   			
   			messages: {
   				"email": {
                          required: "Please enter your email-id."
                      }
   			},
                  
                  submitHandler: function(form) {
                      form.submit();
                  }
              });
          }
      }
   
      //when the dom has loaded setup form validation rules
      $(D).ready(function($) {
          JQUERY4U.UTIL.setupFormValidation();
      });
   
   })(jQuery, window, document);
   
</script>

