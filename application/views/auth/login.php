<!-- Slider-->
<div data-ride="carousel" class="carousel slide banner" id="myCarousel">
   <div class="container-fluid padding">
      <img src="<?php echo base_url(); ?>assets/designs/images/loginbanner1.jpg" width="100%" height="150px">
   </div>
</div>
<!-- Slider-->


 <!-- ##### Contact Area Start ##### -->
    <section class="contact-area">
        <div class="container">
            <div class="row">
                
                <!-- Contact Form -->
                <div class="col-12 col-lg-12">
                    <div class="contact-form">
                        <h4>SIGN <span class="block"> IN</span></h4>
                        
                       
                       
         <div class="col-md-12 formgro">
            <!--  <div id="infoMessage"><?php  echo $message;?></div> -->
            <?php echo $this->session->flashdata('message'); ?>
            <?php echo form_open("auth/login",'class="form-signin" id="login_form"');?>
            <div class="form-group paddin-cont">
               <label class="col-lg-3 control-label" for="ftname">User Email<span style="color:red;">*</span></label>
               <div class="col-lg-9 ">
                  <?php echo form_input($identity);?>
               </div>
            </div>
            <div class="form-group paddin-cont">
               <label class="col-lg-3 control-label" for="ftname">Password<span style="color:red;">*</span></label>
               <div class="col-lg-9 ">
                  <?php echo form_input($password);?>
               </div>
            </div>
            <div style="margin-left:60px;" class="form-group ">
               <div class="col-lg-offset-1 col-lg-10 padd">
                  <?php echo form_submit('submit', $this->lang->line('login_submit_btn'),'class="btn btn-lg btn-primary butt"');?>
               </div>
            </div>
            <?php echo form_close();?>
            <div class="text-center">
               <ul class="btn-group">
                  <li><a href="<?php echo base_url(); ?>auth/forgot_password" class=" btn btn-info"><?php echo lang('login_forgot_password'); ?></a></li>
                  <li> <a href="<?php echo base_url(); ?>auth/create_user" class=" btn btn-warning"> <?php echo lang('signup_user_submit_btn'); ?></a></li>
               </ul>
            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Contact Area End ##### -->



<!-- Validations -->
<script src="<?php echo base_url();?>assets/designs/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/designs/js/jquery.validate.min.js"></script>
<script type="text/javascript"> 
   (function($,W,D)
   {
      var JQUERY4U = {};
   
      JQUERY4U.UTIL =
      {
          setupFormValidation: function()
          {
              //form validation rules
              $("#login_form").validate({
                  rules: {
   				identity: {
                          required: true,
   					email: true
                      },
   				password:{
   					required:true
   				}
                  },
   			
   			messages: {
   				identity: {
                          required: "Please enter your email-id."
                      },
   				password:{
   					required: "Please enter password."
   				}
   			},
                  
                  submitHandler: function(form) {
                      form.submit();
                  }
              });
          }
      }
   
      //when the dom has loaded setup form validation rules
      $(D).ready(function($) {
          JQUERY4U.UTIL.setupFormValidation();
      });
   
   })(jQuery, window, document);
   
</script>

