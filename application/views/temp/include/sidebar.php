<?php 
$cur_tab = $this->uri->segment(2)==''?'dashboard': $this->uri->segment(2);  
?>  

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= base_url() ?>public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= ucwords($this->session->userdata('name')); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      

      <ul class="sidebar-menu" >
      <li class="dropdown-submenu "> <a href="<?php echo base_url();?>">Home</a> </li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="dashboard") echo "active";?>"> <a href="<?php echo base_url();?>admin">Dashboard</a> </li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="categories") echo "active";?>"> 
    <a href="<?php echo base_url();?>admin/categories">Categories</a> </li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="subcategories") echo "active";?>"> 
    <a href="<?php echo base_url();?>admin/subcategories">Sub Categories</a> </li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="subjects") echo "active";?>"> 
    <a  href="<?php echo base_url();?>admin/subjects">Topics</a> </li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="questions") echo "active";?>"> 
    <a href="<?php echo base_url();?>admin/questionsindex">Questions</a></li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="quiz") echo "active";?>"> 
    <a href="<?php echo base_url();?>admin/quiz">Quiz</a></li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="notifications") echo "active";?>"> 
    <a href="<?php echo base_url();?>admin/notifications">Notifications</a></li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="testimonials") echo "active";?>"> 
    <a href="<?php echo base_url();?>admin/testimonials">Testimonials</a></li>
    
    <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="payment_report") echo "active";?>"> 
    <a href="<?php echo base_url();?>admin/payreport">Payment Report</a></li>

   
    <li class="dropdown-submenu"> 
      <a href="<?php echo base_url();?>admin/viewAllUsers">Users</a></li>
    <!--  <li class="dropdown-submenu"> 
      <a href="<?php echo base_url();?>admin/admins">Admins</a></li>  -->
      
    
    </li>
    
    
      <li class="treeview <?php if(isset($active_menu) && $active_menu=="tings") echo "active";?>">  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Settings </a> 
    <ul class="treeview-menu">  
    <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="settings") echo "active";?>"> 
      <a href="<?php echo base_url();?>admin/settings">General Settings</a></li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="email-settings") echo "active";?>"> 
      <a href="<?php echo base_url();?>admin/emailSettings">Email Settings</a></li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="paypal") echo "active";?>"> 
      <a href="<?php echo base_url();?>admin/paypal_settings">Paypal Settings</a></li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="change_password") echo "active";?>"> 
      <a  href="<?php echo base_url();?>auth/change_password">Change Password</a> </li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="aboutus_content") echo "active";?>"> 
      <a href="<?php echo base_url();?>admin/aboutusContent">Aboutus Content</a></li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="groups_settings") echo "active";?>"> 
      <a href="<?php echo base_url();?>admin/group_settings">Groups Settings</a></li>
      
    </ul>
    
    </li>
   
   </ul>


    </section>
    <!-- /.sidebar -->
  </aside>

  
<script>
  $("#<?= $cur_tab; ?>").addClass('active');
</script>
