<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *Must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Clever - Education &amp; Courses Template | Home</title>

    <!-- Favicon -->
    <link rel="icon" href="<?php echo base_url();?>frontassets/img/core-img/favicon.ico">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url();?>frontassets/style.css">

</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="spinner"></div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">

        <!-- Top Header Area -->
        <div class="top-header-area d-flex justify-content-between align-items-center">
            <!-- Contact Info -->
            <div class="contact-info">
                <a href="#"><span>Phone:</span> +44 300 303 0266</a>
                <a href="#"><span>Email:</span> info@clever.com</a>
            </div>
            <!-- Follow Us -->
            <div class="follow-us">
                <span>Follow us</span>
                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            </div>
        </div>

        <!-- Navbar Area -->
        <div class="clever-main-menu">
            <div class="classy-nav-container breakpoint-off">
                <!-- Menu -->
                <nav class="classy-navbar justify-content-between" id="cleverNav">

                    <!-- Logo -->
                    <!-- <a class="nav-brand" href="<?php echo base_url();?>welcome/index"><img src="<?php echo base_url();?>frontassets/img/core-img/logo.png" alt=""></a> -->

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">

                        <!-- Close Button -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul>
                                <li><a href="<?php echo base_url();?>welcome/index">Home</a></li>
                                <!-- <li><a href="#">Pages</a>
                                    <ul class="dropdown">
                                        <li><a href="<?php echo base_url();?>welcome/index">Home</a></li>
                                        <li><a href="courses.html">Courses</a></li>
                                        <li><a href="single-course.html">Single Courses</a></li>
                                        <li><a href="instructors.html">Instructors</a></li>
                                        <li><a href="blog.html">Blog</a></li>
                                        <li><a href="blog-details.html">Single Blog</a></li>
                                        <li><a href="regular-page.html">Regular Page</a></li>
                                        <li><a href="contact.html">Contact</a></li>
                                    </ul>
                                </li> -->

                                <?php

                                echo create_menu();
                                ?>
                               
                                
                                <!-- <li><a href="<?php echo base_url();?>welcome/courses">Courses</a></li> -->
                               
                                <li><a href="<?php echo base_url();?>welcome/contact">Contact</a></li>
                            </ul>

                            <!-- Search Button -->
                            <div class="search-area">
                                <form action="#" method="post">
                                    <input type="search" name="search" id="search" placeholder="Search">
                                    <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </form>
                            </div>

                             <?php $this->load->library('ion_auth');        
                              if ($this->ion_auth->logged_in() && !($this->ion_auth->is_admin() || $this->ion_auth->is_moderator()))
                              {
                              ?>
                           <li class="dropdown <?php if(isset($active_menu) && $active_menu=="dashboard") echo "active";?>">
                              <a href="<?php echo base_url();?>user" class="dropdown-toggle menu-drop" data-toggle="dropdown">Dashboard<b class="caret"></b></a>
                              <ul class="dropdown-menu menu-drop">
                                 <li><a href="<?php echo base_url();?>user">My Dashboard</a></li>
                                 <li><a href="<?php echo base_url();?>user/profile">Profile</a></li>
                                 <li><a href="<?php echo base_url();?>user/quiz_history">Quiz History</a></li>
                                 <li><a href="<?php echo base_url();?>auth/logout">Logout</a></li>
                              </ul>
                           </li>
                            <?php } else if ($this->ion_auth->logged_in() && ($this->ion_auth->is_admin() || $this->ion_auth->is_moderator())) { ?>
                           <li class="dropdown">
                              <a href="<?php echo base_url();?>user" class="dropdown-toggle menu-drop" data-toggle="dropdown">Dashboard<b class="caret"></b></a>
                              <ul class="dropdown-menu menu-drop">
                                 <li><a href="<?php echo base_url();?>admin">My Dashboard</a></li>
                                 <li><a href="<?php echo base_url();?>auth/logout">Logout</a></li>
                              </ul>
                           </li>
                           <?php }else{ ?>



                            <!-- Register / Login -->
                            <div class="register-login-area">
                                <a href="<?php echo base_url();?>auth/create_user" class="btn">Register</a>
                                <a href="<?php echo base_url();?>auth/login" class="btn active">Login</a>
                            </div>

                        <?php } ?>


                            <!-- <div class="login-state d-flex align-items-center">
                                <div class="user-name mr-30">
                                    <div class="dropdown">
                                        <a class="dropdown-toggle" href="#" role="button" id="userName" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Megan Fox</a>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userName">
                                            <a class="dropdown-item" href="#">Profile</a>
                                            <a class="dropdown-item" href="#">Account Info</a>
                                            <a class="dropdown-item" href="<?php echo base_url();?>welcome/index">Logout</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="userthumb">
                                    <img src="img/bg-img/t1.png" alt="">
                                </div>
                            </div> -->

                        </div>
                        <!-- Nav End -->
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->
