

<?php 
$cur_tab = $this->uri->segment(2)==''?'dashboard': $this->uri->segment(2);  
?>  

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= base_url() ?>public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= ucwords($this->session->userdata('name')); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      

       <ul class="sidebar-menu" role="menu" aria-labelledby="dropdownMenu" >
      <li class="dropdown-submenu "> <a  href="<?php echo base_url();?>">Home</a> </li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="dashboard") echo "active";?>"> <a  href="<?php echo base_url();?>user">Dashboard</a> </li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="exams") echo "active";?>"> <a  href="<?php echo base_url();?>user/quizzes">Quizzes</a> </li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="payment_history") echo "active";?>"> <a  href="<?php echo base_url();?>user/payment_history">Payment Hisroty</a> </li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="quiz_history") echo "active";?>"> <a  href="<?php echo base_url();?>user/quiz_history">Quiz Hisroty</a> </li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="profile") echo "active";?>"> <a  href="<?php echo base_url();?>user/profile">My Profile</a> </li>
      <li class="dropdown-submenu <?php if(isset($active_menu) && $active_menu=="change_password") echo "active";?>"> <a  href="<?php echo base_url();?>auth/change_password">Change Password</a> </li>
   </ul>


    </section>
    <!-- /.sidebar -->
  </aside>

  
<script>
  $("#<?= $cur_tab; ?>").addClass('active');
</script>
