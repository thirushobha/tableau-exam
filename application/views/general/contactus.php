<!-- Slider-->
<div data-ride="carousel" class="carousel slide banner" id="myCarousel">
   <div class="container-fluid padding">
      <img src="<?php echo base_url(); ?>assets/designs/images/contact-us-banner.png" width="100%">
   </div>
</div>
<!-- Slider-->



    <!-- ##### Contact Area Start ##### -->
    <section class="contact-area">
        <div class="container">
            <div class="row">
                <!-- Contact Info -->
                <div class="col-12 col-lg-6">
                    <div class="contact--info mt-50 mb-100">
                        <h4>Contact Info</h4>
                        <ul class="contact-list">
                            <li>
                                <h6><i class="fa fa-clock-o" aria-hidden="true"></i> Business Hours</h6>
                                <h6>8:00 AM - 21:00 PM</h6>
                            </li>
                            <li>
                                <h6><i class="fa fa-phone" aria-hidden="true"></i> Number</h6>
                                <h6>+91 9030706500, 04048521656</h6>
                            </li>
                            <li>
                                <h6><i class="fa fa-envelope" aria-hidden="true"></i> Email</h6>
                                <h6>info@4junctionsinstitute.com</h6>
                            </li>
                            <li>
                                <h6><i class="fa fa-map-pin" aria-hidden="true"></i> Address</h6>
                                <h6>1st Floor,102 Balaji Towers,<br>
Prime Hospital Lane, <br>Ameerpet, Hyderabad,<br>
Telangana 500038</h6>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- Contact Form -->
                <div class="col-12 col-lg-6">
                    <div class="contact-form">
                        <h4>Get In Touch</h4>
                         <?php echo $this->session->flashdata('message');?>
                       
                        <form novalidate id="contact_form" name="myForm1" method="POST" action="<?php echo base_url();?>welcome/contact_request_sent" role="form" class="form-horizontal">
      
      <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
        <div class="row">
               <div class="col-12 col-lg-6">
                <div class="form-group paddin-cont">
                  <!-- <label class="col-lg-3 control-label" for="ftname">Name <span style="color:red;">*</span></label> -->
                  <!-- <div class="col-lg-9"> -->
                     <input type="text" placeholder="Name" class="form-control" id="name" name="name">
                  <!-- </div> -->
               </div>
               </div>
               <div class="col-12 col-lg-6">
                <div class="form-group paddin-cont">
                  <!-- <label class="col-lg-3 control-label" for="emailid">Email <span style="color:red;">*</span></label> -->
                  <!-- <div class="col-lg-9"> -->
                     <input type="text" placeholder="Email" id="email" class="form-control" name="email">
                  <!-- </div> -->
               </div>
               </div>
               <div class="col-12 col-lg-6">
                <div class="form-group paddin-cont">
                  <!-- <label class="col-lg-3 control-label" for="mob">Phone <span style="color:red;">*</span></label> -->
                  <!-- <div class="col-lg-9"> -->
                     <input type="text" placeholder="phone" id="phone" class="form-control" name="phone" maxlength="11">
                  <!-- </div> -->
               </div>
              </div>


               <input type="hidden" name="address" id="address" value="hyderabd">
               <div class="col-12 col-lg-6">
                <div class="form-group paddin-cont">
                  <!-- <label class="col-lg-3 control-label" for="sub">Subject <span style="color:red;">*</span></label> -->
                  <!-- <div class="col-lg-9"> -->
                     <input type="text" placeholder="Subject" id="subject" class="form-control" name="subject">
                  <!-- </div> -->
               </div>
               </div>
               <div class="col-12 col-lg-12">
                <div class="form-group paddin-cont">
                  <!-- <label class="col-lg-3 control-label" for="mes">Message</label> -->
                  <!-- <div class="col-lg-9"> -->
                     <textarea rows="3" class="form-control" name="msg" placeholder="Message"></textarea>
                  <!-- </div> -->
               </div>
               </div>
               <div style="margin-left:60px;" class="form-group ">
                  <div class="col-lg-offset-2 col-lg-10">
                     <input type="submit" value="Submit" name="submit" class="btn btn-danger butt">
                  </div>
               </div>

             </div>
            </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Contact Area End ##### -->


<!-- Validations -->
<script src="<?php echo base_url();?>assets/designs/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/designs/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/designs/js/additional-methods.min.js"></script>
<script type="text/javascript"> 
   (function($,W,D)
   {
      var JQUERY4U = {};
   
      JQUERY4U.UTIL =
      {
          setupFormValidation: function()
          {
              //Additional Methods			
   		$.validator.addMethod("lettersonly",function(a,b){return this.optional(b)||/^[a-z ]+$/i.test(a)},"Please enter valid name.");
   					
   		$.validator.addMethod("phoneNumber", function(uid, element) {
   			return (this.optional(element) || uid.match(/^([0-9]*)$/));
   		},"Please enter a valid number.");			
   		
   		
   		//form validation rules
              $("#contact_form").validate({
                  rules: {
                      name: {
                          required: true,
                          lettersonly: true,
   					rangelength: [3, 30]
                      },
                      email: {
                          required: true,
   					email: true
                      },
   				phone: {
                          required: true,
   					phoneNumber: true,
   					rangelength: [10, 11]
                      },
   				
   				subject:{
   					required:true
   				}				
                  },
   			
   			messages: {
   				name: {
                          required: "Please enter your name."
                      },
                      email: {
                          required: "Please enter your email-id."
                      },
   				phone: {
                          required: "Please enter your number."
                      },
   				
   				subject:{
   					required: "Please enter the purpose of contacting."
   				}
   			},
                  
                  submitHandler: function(form) {
                      form.submit();
                  }
              });
          }
      }
   
      //when the dom has loaded setup form validation rules
      $(D).ready(function($) {
          JQUERY4U.UTIL.setupFormValidation();
      });
   
   })(jQuery, window, document);
   
</script>

