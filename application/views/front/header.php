<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="<?php if(isset($site_data->site_keywords)) echo $site_data->site_keywords; else echo ""; ?>">
      <meta name="description" content="<?php if(isset($site_data->site_description)) echo $site_data->site_description; else echo ""; ?>">
      <title><?php if(isset($title)) echo $title." - 4junctionsinstitute"; else echo $site_data->site_title; ?></title>

    <!-- Favicon -->
    <link rel="icon" href="<?php echo base_url();?>assets/designs/images/favicon.png">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url();?>frontassets/style.css">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url();?>frontassets/css/font-awesome.min.css">


        <!-- jQuery-2.2.4 js -->
    <script src="<?php echo base_url();?>frontassets/js/jquery/jquery-2.2.4.min.js"></script>
     <?php if(isset($site_data->google_analytics)) echo $site_data->google_analytics; ?>
     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->


<!-- Added for CR-2424, To disable right click option for the entire site -->
<!-- <script language="javascript">
    var message = "This function is not allowed here.";
    function clickIE4() {

        if (event.button == 2) {
            alert(message);
            return false;
        }
    }

    function clickNS4(e) {
        if (document.layers || document.getElementById && !document.all) {
            if (e.which == 2 || e.which == 3) {
                alert(message);
                return false;
            }
        }
    }

    if (document.layers) {
        document.captureEvents(Event.MOUSEDOWN);
        document.onmousedown = clickNS4;
    }

    else if (document.all && !document.getElementById) {
        document.onmousedown = clickIE4;
    }

    document.oncontextmenu = new Function("alert(message);return false;");

    $(document).keydown(function (event) {
    if (event.keyCode == 123 || event.ctrlKey || event.shiftKey ) { // Prevent F12
        alert("This function is not allowed here. ");
    } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
        return false;
    }
});
</script> -->



<!-- Added for CR-2424, To disable right click option for the entire site: ends here-->


</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="spinner"></div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">

        <!-- Top Header Area -->
        <div class="top-header-area d-flex justify-content-between align-items-center">
            <!-- Contact Info -->
            <div class="contact-info">
                <a href="#"><span>Phone:</span> +91 9030706500, 040-48521656</a>
                <a href="mailto: info@4junctionsinstitute.com"><span>Email:</span>info@4junctionsinstitute.com</a>
            </div>
            <!-- Follow Us -->
            <div class="follow-us">
                <span>Follow us</span>
                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            </div>
        </div>

        <!-- Navbar Area -->
        <div class="clever-main-menu">
            <div class="classy-nav-container breakpoint-off">
                <!-- Menu -->
                <nav class="classy-navbar justify-content-between" id="cleverNav">

                    <!-- Logo -->
                    
                    <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/designs/images/<?php echo $site_data->site_logo; ?>" align="center" width="80%" class="nav-brand" ></a>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">

                        <!-- Close Button -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul>
                                <li><a href="<?php echo base_url();?>welcome/index">Home</a></li>
                                <!-- <li><a href="#">Pages</a>
                                    <ul class="dropdown">
                                        <li><a href="<?php echo base_url();?>welcome/index">Home</a></li>
                                        <li><a href="courses.html">Courses</a></li>
                                        <li><a href="single-course.html">Single Courses</a></li>
                                        <li><a href="instructors.html">Instructors</a></li>
                                        <li><a href="blog.html">Blog</a></li>
                                        <li><a href="blog-details.html">Single Blog</a></li>
                                        <li><a href="regular-page.html">Regular Page</a></li>
                                        <li><a href="contact.html">Contact</a></li>
                                    </ul>
                                </li> -->
                                <li><a href="<?php echo base_url();?>welcome/courses">All Courses</a></li>

                                <?php

                                echo create_menu();
                                ?>
                               
                               
                                <!-- <li><a href="<?php echo base_url();?>welcome/courses">Courses</a></li> -->
                                <li><a href="<?php echo base_url();?>welcome/aboutus">About us</a></li>
                               
                                <li><a href="<?php echo base_url();?>welcome/contact">Contact</a></li>



                                  <?php $this->load->library('ion_auth');        
                              if ($this->ion_auth->logged_in() && !($this->ion_auth->is_admin() || $this->ion_auth->is_moderator()))
                              {
                              ?>
                            <li><a href="#">Dashboard</a>
                                    <ul class="dropdown">
                                       <li><a href="<?php echo base_url();?>user">My Dashboard</a></li>
                                 <li><a href="<?php echo base_url();?>user/profile">Profile</a></li>
                                 <li><a href="<?php echo base_url();?>user/paidcourses">Purchased Courses</a></li>
                                 <li><a href="<?php echo base_url();?>user/quiz_history">Quiz History</a></li>
                                 <li><a href="<?php echo base_url();?>auth/logout">Logout</a></li>
                                    </ul>
                                </li>
                            <?php } else if ($this->ion_auth->logged_in() && ($this->ion_auth->is_admin() || $this->ion_auth->is_moderator())) { ?>

                              <li><a href="#">Dashboard</a>
                                    <ul class="dropdown">
                                       <li><a href="<?php echo base_url();?>admin">My Dashboard</a></li>
                                 <li><a href="<?php echo base_url();?>auth/logout">Logout</a></li>
                                    </ul>
                                </li>


                           
                           <?php }else{ ?>



                          
                                <li><a href="<?php echo base_url();?>auth/create_user" class="btn">Register</a></li>
                                <li><a href="<?php echo base_url();?>auth/login" class="btn active">Login</a></li>
                           

                        <?php } ?>




                            </ul>

                            

                           


                            <!-- <div class="login-state d-flex align-items-center">
                                <div class="user-name mr-30">
                                    <div class="dropdown">
                                        <a class="dropdown-toggle" href="#" role="button" id="userName" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Megan Fox</a>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userName">
                                            <a class="dropdown-item" href="#">Profile</a>
                                            <a class="dropdown-item" href="#">Account Info</a>
                                            <a class="dropdown-item" href="<?php echo base_url();?>welcome/index">Logout</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="userthumb">
                                    <img src="img/bg-img/t1.png" alt="">
                                </div>
                            </div> -->

                        </div>
                        <!-- Nav End -->
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->
