<?php
/**
 * Function Name
 *
 * Function Description
 *
 * @access	public
 * @param	type	name
 * @return	type	
 */
 
if (! function_exists('create_menu'))
{
	function create_menu()
	{
		$CI =& get_instance();
		$cats = $CI->db->get_where('categories',array('status'=>'Active'))->result_array();
		foreach ($cats as $value) {

			$subcats_count = $CI->db->get_where('subcategories',array('catid'=>$value['catid'],'status'=>'Active'));
			if($subcats_count->num_rows()>0){
			# code...
		// echo '<li><a href="'.base_url().'welcome/courses">'.$value['name'].'</a></li>';
		echo '<li class="cn-dropdown-item has-down"><a href="'.base_url().'welcome/courses/'.$value["catid"].'" >'.$value['name'].'</a>';
		$subcats = $subcats_count->result_array();


		echo '<ul class="dropdown"> ';
                                        foreach ($subcats as $subvalue) {
                                        	echo '<li><a href="'.base_url().'welcome/singlecourse/'.$value["catid"].'/'.$subvalue["subcatid"].'">'.$subvalue['name'].'</a></li>';
                                        }
                                    echo '</ul>
                                <span class="dd-trigger"></span>';


		echo '</li>';
	}
		}


	}
}